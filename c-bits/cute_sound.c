#include <SDL2/SDL.h>
#define CUTE_SOUND_FORCE_SDL
#define CUTE_SOUND_IMPLEMENTATION
#include "cute_sound.h"

const char* cs_adapter_error_reason() {
  return cs_error_reason;
}

void cs_adapter_load_wav(cs_loaded_sound_t* out, const char* path) {
  *out = cs_load_wav(path);
}

// LOW-LEVEL API
void cs_adapter_make_playing_sound(cs_playing_sound_t* out, cs_loaded_sound_t* loaded) {
  *out = cs_make_playing_sound(loaded);
}

// HIGH-LEVEL API
cs_playing_sound_t* cs_adapter_play_sound(cs_context_t* ctx, cs_play_sound_def_t* def) {
  return cs_play_sound(ctx, *def);
}

void cs_adapter_make_def(cs_play_sound_def_t *out, cs_loaded_sound_t* sound) {
  *out = cs_make_def(sound);
}

void cs_util_init_def(cs_play_sound_def_t *out, cs_loaded_sound_t* sound, int paused, int looped, float volume_left, float volume_right, float pan, float delay) {
  out->loaded = sound;
  out->paused = paused;
  out->looped = looped;
  out->volume_left=volume_left;
  out->volume_right=volume_right;
  out->pan=pan;
  out->delay=delay;
}

// getters
int cs_is_looped(cs_playing_sound_t* sound) {
  return sound->looped;
}

int cs_is_paused(cs_playing_sound_t* sound) {
  return sound->paused;
}
