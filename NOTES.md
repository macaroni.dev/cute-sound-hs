```
Build profile: -w ghc-8.8.3 -O1
In order, the following will be built (use -v for more details):
 - cute-sound-0.1.0.0 (lib) (first run)
Preprocessing library for cute-sound-0.1.0.0..
GHCi, version 8.8.3: https://www.haskell.org/ghc/  :? for help
ghc: panic! (the 'impossible' happened)
  (GHC version 8.8.3 for x86_64-unknown-linux):
        Loading temp shared object failed: /run/user/1000/ghc10710_0/libghc_1.so: undefined symbol: SDL_InitSubSystem

Please report this as a GHC bug:  https://www.haskell.org/ghc/reportabug

cabal: repl failed for cute-sound-0.1.0.0.
```

This happened when I was trying to `#include` SDL2 and had it in scope (with `nix-shell -p SDL2.dev`), but forget to add it to `pkgconfig-depends`.

----

- [ ] Remove printfs from cs_play_sound
- [ ] It runs..but doesn't make a sound! Why? Maybe SDL just needs to be initialized?