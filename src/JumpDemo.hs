{-# LANGUAGE RankNTypes #-}
{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE TypeApplications #-}
{-# LANGUAGE LambdaCase #-}

module Main(main) where

import qualified SDL

import qualified Cute.Sound.C as CS

import Control.Monad (forever, when, (>=>))
import Control.Concurrent (forkIO, threadDelay)
import Foreign.Marshal.Alloc
import Foreign.Storable
import Foreign.Ptr
import Foreign.C.String
import System.IO
import System.Exit
import System.Environment

allocaEmpty :: forall a b. Storable a => (Ptr a -> IO b) -> IO b
allocaEmpty = allocaBytes (sizeOf (undefined :: a))

putCStrLn :: CString -> IO ()
putCStrLn = peekCString >=> putStrLn

main :: IO ()
main = do
  hSetBuffering stdout NoBuffering
  hSetBuffering stderr NoBuffering

  getArgs >>= \case
    ["naughty"] -> do
      SDL.initializeAll
      ctx <- CS.makeContext nullPtr 44100 15 5 nullPtr
      when (ctx == nullPtr) $ do
        putStrLn "CS.makeContext failed"
        CS.errorReason >>= putCStrLn
        exitFailure

      CS.spawnMixThread ctx
      allocaEmpty @CS.LoadedSound $ \audio_src_jump -> do
        putStrLn $ "audio_src_jump ptr = " ++ show audio_src_jump
        withCString "jump.wav" $ \path -> CS.loadWav audio_src_jump path

        -- Cast the Ptr to a different type and see what's there
        peek @CS.PlaySoundDef (castPtr audio_src_jump) >>= \jd -> putStrLn $ "naughty1 jump_def = " ++ show jd
        
        jump_def <- CS.badMakeDef audio_src_jump
        putStrLn $ "jump_def ptr = " ++ show jump_def
        -- Our old Ptr and this new one are pointing at the same place
        peek @CS.PlaySoundDef (castPtr audio_src_jump) >>= \jd -> putStrLn $ "naughty2 jump_def = " ++ show jd
        peek jump_def >>= \jd -> putStrLn $ "jump_def = " ++ show jd

    ["high-level-thread"] -> do
      SDL.initializeAll
      ctx <- CS.makeContext nullPtr 44100 15 5 nullPtr
      when (ctx == nullPtr) $ do
        putStrLn "CS.makeContext failed"
        CS.errorReason >>= putCStrLn
        exitFailure

      CS.spawnMixThread ctx
      allocaEmpty @CS.LoadedSound $ \audio_src_jump -> do
        putStrLn $ "audio_src_jump ptr = " ++ show audio_src_jump
        withCString "jump.wav" $ \path -> CS.loadWav audio_src_jump path

        allocaEmpty @CS.PlaySoundDef $ \jump_def -> do
          don't $ CS.makeDef jump_def audio_src_jump
          CS.initDef jump_def audio_src_jump
            0 -- paused
            1 -- looped
            1.0 -- volume_left
            1.0 -- volume_right
            0.5 -- pan
            0.5 -- delay
          jump_sound <- CS.playSound ctx jump_def
          forkIO $ forever $ do
            threadDelay 1000
            CS.lock ctx
            CS.isActive jump_sound >>= \active -> putStr $ "\r" ++ show active
            CS.unlock ctx
          threadDelay 1000000
        
    ["low-level-thread"] -> do
      SDL.initializeAll
  
      ctx <- CS.makeContext nullPtr 44100 15 0 nullPtr
      when (ctx == nullPtr) $ do
        putStrLn "CS.makeContext failed"
        CS.errorReason >>= putCStrLn
        exitFailure

      CS.spawnMixThread ctx

      allocaEmpty @CS.LoadedSound $ \audio_src_jump -> do
        withCString "jump.wav" $ \path -> CS.loadWav audio_src_jump path

        putStrLn $ "audio_src_jump ptr = " ++ show audio_src_jump
        peek @CS.PlaySoundDef (castPtr audio_src_jump) >>= \jd -> putStrLn $ "naughty jump_def = " ++ show jd
        
        allocaEmpty @CS.PlayingSound $ \jump_sound -> do
          CS.makePlayingSound jump_sound audio_src_jump

          CS.insertSound ctx jump_sound
          
          forkIO $ forever $ do
            threadDelay 1000
            CS.isActive jump_sound >>= \active -> putStr $ "\r" ++ show active
          threadDelay 1000000
    args -> die $ "Unknown args: " ++ show args
  
don't :: IO a -> IO ()
don't = const (pure ())
