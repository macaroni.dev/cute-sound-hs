{-# LANGUAGE LambdaCase #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE TypeApplications #-}

module Main where

import qualified Cute.Sound.C as CS

import Control.Monad (when, (>=>))
import Data.Function (fix)
import Data.Foldable (for_)

import Control.Concurrent (threadDelay)
import System.Exit
import System.Environment
import System.IO

import Foreign.C.String
import Foreign.C.Types
import Foreign.Marshal.Alloc
import qualified Foreign.Marshal.Utils as C
import Foreign.Ptr

import qualified SDL

main :: IO ()
main = do
  hSetBuffering stdout NoBuffering
  hSetBuffering stderr NoBuffering
  putStrLn "hello bgm"

  SDL.initializeAll
  window <- SDL.createWindow "BGM" SDL.defaultWindow
  
  ctx <- CS.makeContext nullPtr 44100 15 0 nullPtr
  when (ctx == nullPtr) $ do
    putStrLn "CS.makeContext failed"
    CS.errorReason >>= putCStrLn
    exitFailure

  bgm_loaded <- malloc @CS.LoadedSound
  withCString "bgm.wav" $ CS.loadWav bgm_loaded
  -- todo: error handling

  bgm_sound <- malloc @CS.PlayingSound
  CS.makePlayingSound bgm_sound bgm_loaded
  CS.loopSound bgm_sound 1

  CS.spawnMixThread ctx
  
  fix $ \loop -> do
    SDL.waitEvent >>= \e -> case SDL.eventPayload e of
      (SDL.KeyboardEvent ke@SDL.KeyboardEventData{SDL.keyboardEventKeyMotion=SDL.Pressed}) ->
        case SDL.keysymKeycode $ SDL.keyboardEventKeysym ke of
          -- TODO: Key logic
          SDL.KeycodeQ -> do
            CS.insertSound ctx bgm_sound
            loop
          SDL.KeycodeW -> do
            CS.isPaused bgm_sound >>= CS.pauseSound bgm_sound . c_not
            loop
          SDL.KeycodeE -> do
            CS.stopSound bgm_sound
            CS.makePlayingSound bgm_sound bgm_loaded
            CS.insertSound ctx bgm_sound
            loop
          SDL.KeycodeEscape -> putStrLn "bye!"
      SDL.QuitEvent -> putStrLn "bye!"
      _ -> loop

  CS.stopSound bgm_sound
  free bgm_sound
  free bgm_loaded

c_not :: CBool -> CBool
c_not = C.fromBool . not . C.toBool

putCStrLn :: CString -> IO ()
putCStrLn = peekCString >=> putStrLn
