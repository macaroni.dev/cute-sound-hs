{-# LANGUAGE RankNTypes #-}
{-# LANGUAGE StrictData #-}
{-# LANGUAGE RecordWildCards #-}

module Cute.Sound.C where

import Data.Int
import Data.Void
import Data.Word
import Foreign.Ptr
import Foreign.C.String
import Foreign.C.Types
import Foreign.Storable

#include <SDL2/SDL.h>
#include <cute_sound.h>

data LoadedSound
instance Storable LoadedSound where
  alignment _ = #{alignment cs_loaded_sound_t}
  sizeOf _ = #{size cs_loaded_sound_t}

  peek = undefined
  poke = undefined

data PlayingSound
instance Storable PlayingSound where
  alignment _ = #{alignment cs_playing_sound_t}
  sizeOf _ = #{size cs_playing_sound_t}

  peek = undefined
  poke = undefined

data Context

pluginsMax :: forall a. Integral a => a
pluginsMax = #{const CUTE_SOUND_PLUGINS_MAX}

foreign import ccall safe "cute_sound.h cs_adapter_error_reason" errorReason
  :: IO CString

foreign import ccall safe "cute_sound.h cs_adapter_load_wav" loadWav
  :: Ptr LoadedSound -- ^ out
  -> CString
  -> IO ()

foreign import ccall safe "cute_sound.h cs_read_mem_wav" readMemWav
  :: Ptr Void -- ^ memory
  -> #{type int} -- ^ size
  -> Ptr LoadedSound -- ^ sound (out)
  -> IO ()

-- TODO: _ogg functions

foreign import ccall safe "cute_sound.h cs_free_sound" freeSound
  :: Ptr LoadedSound
  -> IO ()

foreign import ccall safe "cute_sound.h cs_sound_size" soundSize
  :: Ptr LoadedSound
  -> IO #{type int}

foreign import ccall safe "cute_sound.h cs_make_context" makeContext
  :: Ptr Void -- ^ hwnd
  -> #{type unsigned} -- ^ play_frequency_in_Hz
  -> #{type int} -- ^ buffered_samples
  -> #{type int} -- ^ playing_pool_count
  -> Ptr Void -- ^ user_allocator_context
  -> IO (Ptr Context)

foreign import ccall safe "cute_sound.h cs_shutdown_context" shutdownContext
  :: Ptr Context
  -> IO ()

foreign import ccall safe "cute_sound.h cs_spawn_mix_thread" spawnMixThread
  :: Ptr Context
  -> IO ()

foreign import ccall safe "cute_sound.h cs_thread_sleep_delay" threadSleepDelay
  :: Ptr Context
  -> #{type int} -- ^ milliseconds
  -> IO ()

foreign import ccall safe "cute_sound.h cs_lock" lock
  :: Ptr Context
  -> IO ()

foreign import ccall safe "cute_sound.h cs_unlock" unlock
  :: Ptr Context
  -> IO ()

foreign import ccall safe "cute_sound.h cs_mix" mix
  :: Ptr Context
  -> IO ()

foreign import ccall safe "cute_sound.h cs_is_active" isActive
  :: Ptr PlayingSound
  -> IO CBool

foreign import ccall safe "cute_sound.h cs_stop_sound" stopSound
  :: Ptr PlayingSound
  -> IO ()

-- Not sure how to do this..we may have to lock? Or manually mix?
--foreign import ccall safe "cute_sound.h cs_rewind_sound" rewindSound
--  :: Ptr PlayingSound
--  -> IO ()

foreign import ccall safe "cute_sound.h cs_loop_sound" loopSound
  :: Ptr PlayingSound
  -> CBool -- ^ zero_for_no_loop
  -> IO ()

foreign import ccall safe "cute_sound.h cs_pause_sound" pauseSound
  :: Ptr PlayingSound
  -> CBool -- ^ one_for_paused
  -> IO ()

foreign import ccall safe "cute_sound.h cs_set_pan" setPan
  :: Ptr PlayingSound
  -> #{type float} -- ^ pan
  -> IO ()

foreign import ccall safe "cute_sound.h cs_set_volume" setVolume
  :: Ptr PlayingSound
  -> #{type float} -- ^ volume_left
  -> #{type float} -- ^ volume_right
  -> IO ()

foreign import ccall safe "cute_sound.h cs_set_delay" setDelay
  :: Ptr Context
  -> Ptr PlayingSound
  -> #{type float} -- ^ delay_in_seconds
  -> IO ()

foreign import ccall safe "cute_sound.h cs_get_playing" getPlaying
  :: Ptr Context
  -> Ptr PlayingSound

foreign import ccall safe "cute_sound.h cs_sleep" sleep
  :: #{type int} -- ^ milliseconds
  -> IO ()

-- LOW-LEVEL API

foreign import ccall safe "cute_sound.h cs_adapter_make_playing_sound" makePlayingSound
  :: Ptr PlayingSound -- ^ out
  -> Ptr LoadedSound -- ^ loaded
  -> IO ()

foreign import ccall safe "cute_sound.h cs_insert_sound" insertSound
  :: Ptr Context
  -> Ptr PlayingSound
  -> IO CBool

-- HIGH-LEVEL API

-- TODO: Use `memorable` when it's ready
data PlaySoundDef = PlaySoundDef
  { paused :: CBool
  , looped :: CBool
  , volume_left :: #{type float}
  , volume_right :: #{type float}
  , pan :: #{type float}
  , delay :: #{type float}
  , loaded :: Ptr LoadedSound
  } deriving (Eq, Show)

instance Storable PlaySoundDef where
  alignment _ = #{alignment cs_play_sound_def_t}
  sizeOf _ = #{size cs_play_sound_def_t}

  peek ptr = do
    paused <- #{peek cs_play_sound_def_t, paused} ptr
    looped <- #{peek cs_play_sound_def_t, looped} ptr
    volume_left <- #{peek cs_play_sound_def_t, volume_left} ptr
    volume_right <- #{peek cs_play_sound_def_t, volume_right} ptr
    pan <- #{peek cs_play_sound_def_t, pan} ptr
    delay <- #{peek cs_play_sound_def_t, delay} ptr
    loaded <- #{peek cs_play_sound_def_t, loaded} ptr
    pure PlaySoundDef{..}

  poke ptr PlaySoundDef{..} = do
    #{poke cs_play_sound_def_t, paused} ptr paused
    #{poke cs_play_sound_def_t, looped} ptr looped
    #{poke cs_play_sound_def_t, volume_left} ptr volume_left
    #{poke cs_play_sound_def_t, volume_right} ptr volume_right
    #{poke cs_play_sound_def_t, pan} ptr pan
    #{poke cs_play_sound_def_t, delay} ptr delay
    #{poke cs_play_sound_def_t, loaded} ptr loaded

foreign import ccall safe "cute_sound.h cs_adapter_play_sound" playSound
  :: Ptr Context
  -> Ptr PlaySoundDef
  -> IO (Ptr PlayingSound)

foreign import ccall safe "cute_sound.h cs_make_def" badMakeDef
  :: Ptr LoadedSound
  -> IO (Ptr PlaySoundDef)

foreign import ccall safe "cute_sound.h cs_adapter_make_def" makeDef
  :: Ptr PlaySoundDef -- ^ out
  -> Ptr LoadedSound
  -> IO ()

foreign import ccall safe "cute_sound.h cs_util_init_def" initDef
  :: Ptr PlaySoundDef -- ^ out
  -> Ptr LoadedSound -- ^ sound
  -> #{type int} -- ^ paused
  -> #{type int} -- ^ looped
  -> #{type float} -- ^ volume_left
  -> #{type float} -- ^ volume_right
  -> #{type float} -- ^ pan
  -> #{type float} -- ^ delay
  -> IO ()

foreign import ccall safe "cute_sound.h cs_stop_all_sounds" stopAllSounds
  :: Ptr Context
  -> IO ()

-- Getters
foreign import ccall safe "cute_sound.h cs_is_looped" isLooped
  :: Ptr PlayingSound
  -> IO CBool -- ^ 1 for looped

foreign import ccall safe "cute_sound.h cs_is_paused" isPaused
  :: Ptr PlayingSound
  -> IO CBool -- ^ 1 for paused
